BEGIN;
-- Dumping database structure for demo_db
CREATE DATABASE IF NOT EXISTS `demo_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci;
USE `demo_db`;


-- Dumping structure for tabell demo_db.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `first_name` varchar(45) COLLATE utf8_swedish_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_swedish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_swedish_ci NOT NULL,
  `phone` varchar(45) COLLATE utf8_swedish_ci NOT NULL,
  PRIMARY KEY (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- Data exporting was unselected.


-- Dumping structure for tabell demo_db.payment
CREATE TABLE IF NOT EXISTS `payment` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `request_id` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `transaction_id` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `amount` double NOT NULL,
  `customer` int(11) NOT NULL,
  PRIMARY KEY (`p_key`),
  KEY `payCustomer` (`customer`),
  CONSTRAINT `payCustomer` FOREIGN KEY (`customer`) REFERENCES `customer` (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- Data exporting was unselected.

-- Dumping structure for tabell demo_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` longtext NOT NULL,
  `role` varchar(15) NOT NULL,
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `usrname` (`username`),
  CONSTRAINT `customerUser` FOREIGN KEY (`p_key`) REFERENCES `customer` (`p_key`)
) ENGINE=InnoDB CHARSET=utf8 COLLATE=utf8_swedish_ci;


-- Dumping structure for tabell demo_db.seat
CREATE TABLE IF NOT EXISTS `seat` (
  `p_key` int(11) NOT NULL AUTO_INCREMENT,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `version` int(11) NOT NULL,
  `seat_number` int(11) NOT NULL,
  `customer` int(11) DEFAULT NULL,
  `seat_lock` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`p_key`),
  UNIQUE KEY `seat_number_UNIQUE` (`seat_number`),
  KEY `INDEX_SEAT` (`seat_number`),
  KEY `customer` (`customer`),
  CONSTRAINT `customer` FOREIGN KEY (`customer`) REFERENCES `customer` (`p_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

DROP PROCEDURE IF EXISTS `init_seat`;

DELIMITER $$
CREATE PROCEDURE init_seat()

BEGIN
  DECLARE seat_number INT DEFAULT 1 ;
  INIT_LOOP: LOOP
    INSERT INTO `seat` (create_date, update_date, version, seat_number) VALUES(NOW(), NOW(), 0, seat_number);
    SET seat_number = seat_number + 1;
    IF seat_number = 101 THEN
      LEAVE INIT_LOOP;
    END IF;
  END LOOP INIT_LOOP;
END $$
DELIMITER ;


CALL `init_seat`;
DROP PROCEDURE `init_seat`;
COMMIT;

