package com.cgi.osd.transactiondemo.persistence.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.cgi.osd.transactiondemo.integration.EmailUtil;
import com.cgi.osd.transactiondemo.integration.EmailUtilBean;
import com.cgi.osd.transactiondemo.integration.EmailUtilMockup;
import com.cgi.osd.transactiondemo.integration.PersistenceManager;
import com.cgi.osd.transactiondemo.integration.PersistenceMangerBeanMockup;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.util.Resources;
import com.cgi.osd.transactiondemo.util.TestImplementation;

/**
 * This class is responsible for testing the persistence layer.
 *
 */
@RunWith(Arquillian.class)
public class PersistenceManagerTest {

    private static final int NUMBER_OF_SEATS = 10;

    @Deployment
    public static Archive<WebArchive> createTestArchive() {
	final WebArchive archive = ShrinkWrap.create(WebArchive.class, "test.war")
		.addClasses(TestImplementation.class, Resources.class, PersistenceMangerBeanMockup.class,
			EmailUtilMockup.class)
		.addPackages(true, "com.cgi.osd.transactiondemo.logic")
		.addPackages(true, "com.cgi.osd.student.paymentserver.soap")
		.addPackages(true, Filters.exclude(EmailUtilBean.class), "com.cgi.osd.transactiondemo.integration")
		.addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
		.addAsWebInfResource("test-ds.xml").addAsWebInfResource("beans-test.xml", "beans.xml");

	return archive;
    }

    @Inject
    @TestImplementation
    private PersistenceManager persistenceManagerMockUp;

    @Inject
    private PersistenceManager persistenceManager;

    @Inject
    private EmailUtil emailUtil;

    @PersistenceContext(unitName = "primary")
    private EntityManager em;

    @Inject
    UserTransaction utx;

    @Before
    public void initDataBase() {
	try {
	    this.utx.begin();
	    this.em.joinTransaction();
	    clearTables();
	    initSeat();
	    this.utx.commit();
	} catch (final NotSupportedException e) {
	    fail("NotSupportedException: " + e.getMessage());
	} catch (final SystemException e) {
	    fail("SystemException: " + e.getMessage());
	} catch (final SecurityException e) {
	    fail("SecurityException: " + e.getMessage());
	} catch (final IllegalStateException e) {
	    fail("IllegalStateException: " + e.getMessage());
	} catch (final RollbackException e) {
	    fail("RollbackException: " + e.getMessage());
	} catch (final HeuristicMixedException e) {
	    fail("HeuristicMixedException: " + e.getMessage());
	} catch (final HeuristicRollbackException e) {
	    fail("HeuristicRollbackException: " + e.getMessage());
	}
    }

    /**
     * This method verifies conversion from seat entity to seat domain object.
     *
     */
    @Test
    public void testMockAllSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = this.persistenceManagerMockUp.getAllSeats();
	for (int i = 0; i < seats.size(); i++) {
	    final SeatDO seat = seats.get(i);
	    assertEquals(i, seat.getSeatNumber());
	}
    }

    /**
     * This test verifies that the test database has been initialized.
     */
    @Test
    public void testDatabaseAllSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = this.persistenceManager.getAllSeats();
	assertEquals(NUMBER_OF_SEATS, seats.size());
    }

    /**
     * This test demonstrates the use aof an alternative implementation.
     */
    @Test
    public void testEmail() {
	this.emailUtil.sendEmail(null, null, null);
    }

    private void clearTables() {
	final Query deleteSeatQuery = this.em.createQuery("DELETE FROM Seat");
	deleteSeatQuery.executeUpdate();
    }

    private void initSeat() {
	final Query insertQuery = this.em
		.createNativeQuery("INSERT INTO seat (create_date, update_date, version, seat_number, seat_lock) "
			+ "VALUES(NOW(), NOW(), 0, :seatNumber, 0);");

	for (int i = 0; i < NUMBER_OF_SEATS; i++) {
	    insertQuery.setParameter("seatNumber", i);
	    insertQuery.executeUpdate();
	}
    }

}
