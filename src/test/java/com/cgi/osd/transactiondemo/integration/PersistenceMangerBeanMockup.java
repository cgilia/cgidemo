package com.cgi.osd.transactiondemo.integration;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;
import com.cgi.osd.transactiondemo.util.TestImplementation;

/**
 * This class is responsible for constituting a test implementation of the
 * PersistenceManager interface.
 */
@TestImplementation
@ApplicationScoped
public class PersistenceMangerBeanMockup implements PersistenceManager {

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Override
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
	final Collection<Seat> seats = new LinkedList<>();
	for (int i = 0; i < 5; i++) {
	    seats.add(new Seat(i));
	}
	final List<SeatDO> seatDOs = this.objectFactory.createSeatDOList(seats);
	return seatDOs;
    }

    @Override
    public int insertNewCustomer(CustomerDO customerDO, UserDO userDO) throws PersistenceOperationException {
	return 0;

	// TODO Auto-generated method stub
    }

    public void booking(int customerId, List<Integer> byCustomerSelectedSeats, PaymentResponseDTO paymentResponseDTO)
	    throws PersistenceOperationException {

    }

    @Override
    public List<SeatDO> getAllSelectedSeats(List<Integer> selectedSeats) {
	return null;
	// TODO Auto-generated method stub

    }

    @Override
    public void deleteBooking(CustomerDO customerDO) {

    }

    @Override
    public List<CustomerDO> getAllbookings() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<CustomerDO> getCustomerByEmail(String userName) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void createBooking(int customerId, List<Integer> byCustomerSelectedSeats,
	    PaymentRequestDTO paymentRequestDTO) throws PersistenceOperationException {
	// TODO Auto-generated method stub

    }

}
