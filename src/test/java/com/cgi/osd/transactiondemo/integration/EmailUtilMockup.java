package com.cgi.osd.transactiondemo.integration;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

@Alternative
public class EmailUtilMockup implements EmailUtil {

    @Inject
    Logger logger;

    @Override
    public void sendEmail(CustomerDO customer, List<SeatDO> bookedSeats, String password) {
	this.logger.info("Email mockup send was called.");
    }

}
