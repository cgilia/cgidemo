package com.cgi.osd.transactiondemo.integration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for creating objects by converting between domain
 * object classes and entity classes.
 *
 * @author anderssonhr
 *
 */
@ApplicationScoped
public class PersistenceObjectFactory {

    @Inject
    PersistenceManager persistenceManager;

    public SeatDO createSeatDO(Seat seat) {
	if (seat == null) {
	    return null;
	}
	final SeatDO seatDO = new SeatDO(seat.getSeatNumber(), seat.getVersion(), seat.getSeatLock());

	return seatDO;
    }

    public List<SeatDO> createSeatDOList(Collection<Seat> seats) {
	final List<SeatDO> result = new ArrayList<>();
	for (final Seat seat : seats) {
	    if (seat != null) {
		result.add(createSeatDO(seat));
	    }
	}
	return result;
    }

    public List<CustomerDO> createCustomerDOList(Collection<Customer> customers) {
	final List<CustomerDO> result = new ArrayList<>();
	for (final Customer customer : customers) {
	    if (customer != null) {
		result.add(createCustomerDO(customer));
	    }
	}
	return result;
    }

    public Customer createCustomer(CustomerDO customerDO) throws PersistenceOperationException {
	if (customerDO != null) {
	    Customer customer = new Customer();
	    customer.setFirstName(customerDO.getFirstName());
	    customer.setLastName(customerDO.getLastName());
	    customer.setEmail(customerDO.getEmail());
	    customer.setPhone(customerDO.getPhone());
	    return customer;
	}
	return null;
    }

    public CustomerDO createCustomerDO(Customer customer) {
	CustomerDO customerDO = new CustomerDO(customer.getFirstName(), customer.getLastName(), customer.getEmail(),
		customer.getPhone(), customer.getCreateDate(), customer.getSeats(), customer.getPayments());
	return customerDO;
    }

    public Payment createPayment(PaymentResponseDTO paymentResponseDTO) {
	Payment payment = new Payment();
	payment.setAmount(paymentResponseDTO.getAmount());
	payment.setRequestId(paymentResponseDTO.getRequestId());
	payment.setTransactionId(paymentResponseDTO.getTransactionId());
	return payment;
    }

    public User createNewUser(UserDO userDO) {
	User user = new User();
	if (userDO != null) {
	    user.setUsername(userDO.getUserName());
	    user.setPassword(userDO.getPassword());
	    return user;
	}
	return null;
    }
}
