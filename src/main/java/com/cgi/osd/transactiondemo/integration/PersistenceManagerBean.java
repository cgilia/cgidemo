package com.cgi.osd.transactiondemo.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

import com.cgi.osd.student.paymentserver.soap.PaymentFault_Exception;
import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for implementing methods for persistence handling.
 * All methods are thread safe.
 *
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NEVER)
@Interceptors(ExceptionInterceptor.class)
public class PersistenceManagerBean implements PersistenceManager {

    @Inject
    private EntityManager em;

    @Inject
    private PersistenceObjectFactory objectFactory;

    @Inject
    RestService restService;

    @Inject
    Logger logger;

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<SeatDO> getAllSeats() {
	final List<Seat> dbResult = fetchAllSeats();
	final List<SeatDO> result = this.objectFactory.createSeatDOList(dbResult);
	return result;
    }

    List<Seat> fetchAllSeats() {
	final TypedQuery<Seat> query = this.em.createNamedQuery("Seat.findAll", Seat.class);
	query.setLockMode(LockModeType.OPTIMISTIC);
	final List<Seat> dbResult = query.getResultList();
	return dbResult;
    }

    public List<Seat> fetchAllSelectedSeats(List<Integer> selectedSeats) {
	final TypedQuery<Seat> querySeats = this.em.createNamedQuery("Seat.findSelectedSeats", Seat.class);
	querySeats.setParameter("seatNumber", selectedSeats);

	final List<Seat> dbResult = querySeats.getResultList();
	return dbResult;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<SeatDO> getAllSelectedSeats(List<Integer> selectedSeats) {
	final List<Seat> dbResult = fetchAllSelectedSeats(selectedSeats);
	final List<SeatDO> result = this.objectFactory.createSeatDOList(dbResult);
	return result;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int insertNewCustomer(CustomerDO customerDO, UserDO userDO) throws PersistenceOperationException {
	final int customerId;
	final Customer customer = this.objectFactory.createCustomer(customerDO);
	final User user = objectFactory.createNewUser(userDO);
	user.setRole("USER");

	em.persist(customer);
	em.persist(user);
	em.flush();
	customerId = customer.getPKey();
	return customerId;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteBooking(CustomerDO customerDO) {
	final TypedQuery<Customer> queryCustomer = this.em.createNamedQuery("Customer.findByEmail", Customer.class);
	queryCustomer.setParameter("email", customerDO.getEmail());

	Customer dbResult = queryCustomer.getSingleResult();

	List<Integer> customerSeats = new ArrayList<>();
	for (Seat seat : dbResult.getSeats()) {
	    customerSeats.add(seat.getSeatNumber());
	}

	final TypedQuery<Seat> querySeat = this.em.createNamedQuery("Seat.findSelectedSeats", Seat.class);
	querySeat.setParameter("seatNumber", customerSeats);

	List<Seat> seats = querySeat.getResultList();
	for (Seat seat : seats) {
	    seat.setSeatLock(0);
	    seat.setCustomerBean(null);
	    em.merge(seat);
	}
	em.remove(dbResult);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void createBooking(int customerId, List<Integer> byCustomerSelectedSeats,
	    PaymentRequestDTO paymentRequestDTO) {
	PaymentResponseDTO paymentResponseDTO = performPayment(paymentRequestDTO);
	final int locked = 1;
	final TypedQuery<Customer> queryCustomer = this.em.createNamedQuery("Customer.findById", Customer.class);
	queryCustomer.setParameter("pKey", customerId);

	final List<Seat> seatList = fetchAllSelectedSeats(byCustomerSelectedSeats);
	for (Seat seat : seatList) {
	    seat.setSeatLock(locked);
	}
	final Customer customer = queryCustomer.getSingleResult();
	final Payment payment = objectFactory.createPayment(paymentResponseDTO);
	customer.setSeats(seatList);
	customer.addPayment(payment);

	for (Seat seat : seatList) {
	    seat.setCustomerBean(customer);
	}
	payment.setCustomerBean(customer);
	em.persist(payment);
	em.merge(customer);
	new LogoutBean().endBooking();
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<CustomerDO> getAllbookings() {
	final List<Customer> dbresult = fetchAllBookings();
	final List<CustomerDO> result = objectFactory.createCustomerDOList(dbresult);
	return result;

    }

    public List<Customer> fetchAllBookings() {
	final TypedQuery<Customer> queryCustomer = this.em.createNamedQuery("Customer.findAll", Customer.class);
	final List<Customer> dbResult = queryCustomer.getResultList();
	return dbResult;

    }

    public List<Customer> fetchCustomerByEmail(String userName) {
	final TypedQuery<Customer> queryCustomer = this.em.createNamedQuery("Customer.findByEmail", Customer.class);
	queryCustomer.setParameter("email", userName);

	final List<Customer> dbResult = queryCustomer.getResultList();
	return dbResult;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<CustomerDO> getCustomerByEmail(String userName) {
	final List<Customer> dbResult = fetchCustomerByEmail(userName);
	final List<CustomerDO> result = this.objectFactory.createCustomerDOList(dbResult);
	return result;
    }

    private PaymentResponseDTO performPayment(PaymentRequestDTO paymentRequestDTO) {
	if ("REST".equals(System.getProperty("paymentService"))) {
	    return restPayment(paymentRequestDTO);
	} else {
	    try {
		return soapPayment(paymentRequestDTO);
	    } catch (PaymentFault_Exception e) {
		e.printStackTrace();
	    }
	}
	logger.severe("Add missing system property for payment service");
	return null;
    }

    private PaymentResponseDTO soapPayment(PaymentRequestDTO paymentRequestDTO) throws PaymentFault_Exception {
	PaymentResponseDTO paymentResponseDTO = new PaymentService().getPaymentServiceSOAP()
		.performPurchase(paymentRequestDTO);
	return paymentResponseDTO;
    }

    private PaymentResponseDTO restPayment(PaymentRequestDTO paymentRequestDTO) {
	PaymentResponseDTO responseDTO = restService.postPayment(paymentRequestDTO);
	return responseDTO;
    }

}
