package com.cgi.osd.transactiondemo.integration;

import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

public interface EmailUtil {

    public void sendEmail(CustomerDO customer, List<SeatDO> bookedSeats, String password);
}
