package com.cgi.osd.transactiondemo.integration;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the seat database table.
 *
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Seat.findAll", query = "SELECT s FROM Seat s"),
	@NamedQuery(name = "Seat.findBySeatNumber", query = "SELECT s FROM Seat s WHERE s.seatNumber = :seatNumber"),
	@NamedQuery(name = "Seat.findSelectedSeats", query = "SELECT s FROM Seat s WHERE s.seatNumber in :seatNumber") })
@Table(name = "seat")
public class Seat extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "seat_number")
    private int seatNumber;

    @Column(name = "seat_lock")
    private int seatLock;

    // bi-directional many-to-one association to Customer
    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customerBean;

    public Seat() {
    }

    public Seat(int seatNumber) {
	super();
	this.seatNumber = seatNumber;
    }

    public int getSeatNumber() {
	return this.seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
	this.seatNumber = seatNumber;
    }

    public int getSeatLock() {
	return this.seatLock;
    }

    public void setSeatLock(int seatLock) {
	this.seatLock = seatLock;
    }

    public Customer getCustomerBean() {
	return this.customerBean;
    }

    public void setCustomerBean(Customer customerBean) {
	this.customerBean = customerBean;
    }
}