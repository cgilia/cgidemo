package com.cgi.osd.transactiondemo.integration;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;

@Stateless
public class EmailUtilBean implements EmailUtil {

    public EmailUtilBean() {

    }

    @Inject
    private Logger logger;

    @Resource(mappedName = "java:jboss/mail/Gmail")
    Session gmailSession;

    @Asynchronous
    @Override
    public void sendEmail(CustomerDO customer, List<SeatDO> bookedSeats, String password) {

	final int price = 200;
	final String toEmail = customer.getEmail();
	final String from = "NoReply-CGI_LIA";
	final String subject = "Bokningsbekräftelse";
	final String content = "Tack för din bokning!" + "\n" + "\n" + customer.getFirstName() + " "
		+ customer.getLastName() + "\n" + customer.getEmail() + "\n" + customer.getPhone() + "\n"
		+ "Bokade platser: " + bookedSeats.size() + "\n" + "Pris: " + bookedSeats.size() * price + "Kr" + "\n"
		+ "\n" + "Inloggningsuppgifter: " + "\n" + "Användarnamn: " + customer.getEmail() + "\n" + "Lösenord: "
		+ password + "\n" + "\n" + "En faktura skickas ut via email inom 30 dagar.";

	logger.info("Sending Email from " + from + " to " + toEmail + " : " + subject);
	try {
	    Message message = new MimeMessage(gmailSession);
	    message.setFrom(new InternetAddress(from));
	    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
	    message.setSubject(subject);
	    message.setText(content);

	    Transport.send(message);
	    logger.info("Email was sent");

	} catch (MessagingException e) {
	    logger.severe("Error while sending email : " + e.getMessage());
	}
    }
}
