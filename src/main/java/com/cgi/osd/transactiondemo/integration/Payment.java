package com.cgi.osd.transactiondemo.integration;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the payment database table.
 *
 */
@Entity
@NamedQuery(name = "Payment.findAll", query = "SELECT p FROM Payment p")
@Table(name = "payment")
public class Payment extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "amount")
    private double amount;

    @Column(name = "request_id")
    private String requestId;

    @Column(name = "transaction_id")
    private String transactionId;

    // bi-directional many-to-one association to Customer
    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customerBean;

    public Payment() {
    }

    public double getAmount() {
	return this.amount;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    @Override
    public Date getCreateDate() {
	return this.createDate;
    }

    @Override
    public void setCreateDate(Date createDate) {
	this.createDate = createDate;
    }

    public String getRequestId() {
	return this.requestId;
    }

    public void setRequestId(String requestId) {
	this.requestId = requestId;
    }

    public String getTransactionId() {
	return this.transactionId;
    }

    public void setTransactionId(String transactionId) {
	this.transactionId = transactionId;
    }

    public Customer getCustomerBean() {
	return this.customerBean;
    }

    public void setCustomerBean(Customer customerBean) {
	this.customerBean = customerBean;
    }

}