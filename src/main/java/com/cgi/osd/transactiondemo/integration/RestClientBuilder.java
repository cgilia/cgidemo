package com.cgi.osd.transactiondemo.integration;

import javax.ejb.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

@Singleton
public class RestClientBuilder {

    private Client client;

    public RestClientBuilder() {
	this.client = ClientBuilder.newClient();
    }

    public Client getClient() {
	return client;
    }
}
