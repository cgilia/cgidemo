package com.cgi.osd.transactiondemo.integration;

import java.util.List;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This interface defines all methods that should be used for persistence
 * operations.
 *
 * @author anderssonhr
 *
 */
public interface PersistenceManager {

    /**
     * This method returns a list of all free seats.
     *
     * @return a list of free seats. The list might be empty.
     * @throws SeatException
     *             when a persistence operation fails.
     */
    List<SeatDO> getAllSeats() throws PersistenceOperationException;

    int insertNewCustomer(CustomerDO customerDO, UserDO userDO) throws PersistenceOperationException;

    public void createBooking(int customerId, List<Integer> byCustomerSelectedSeats, PaymentRequestDTO paymentRequestDTO)
	    throws PersistenceOperationException;

    List<SeatDO> getAllSelectedSeats(List<Integer> selectedSeats);

    void deleteBooking(CustomerDO customerDO);

    List<CustomerDO> getAllbookings();

    List<CustomerDO> getCustomerByEmail(String userName);
}
