package com.cgi.osd.transactiondemo.integration;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;

public interface RestService {

    public PaymentResponseDTO postPayment(PaymentRequestDTO request);
}
