package com.cgi.osd.transactiondemo.integration;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the users database table.
 *
 */
@Entity
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
@Table(name = "users")
public class User extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Lob
    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @Column(name = "username")
    private String username;

    @OneToOne
    @JoinColumn(name = "p_key")
    private Customer customer;

    public User() {
    }

    public String getPassword() {
	return this.password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getRole() {
	return this.role;
    }

    public void setRole(String role) {
	this.role = role;
    }

    public String getUsername() {
	return this.username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public Customer getCustomer() {
	return this.customer;
    }

    public void setCustomer(Customer customer) {
	this.customer = customer;
    }
}