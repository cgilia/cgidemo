package com.cgi.osd.transactiondemo.integration;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.student.paymentserver.soap.PaymentResponseDTO;

@Named
@RequestScoped
@Consumes("application/json")
@Produces("application/json")
public class RestServiceBean implements RestService {

    @Inject
    RestClientBuilder restClientBuilder;

    @POST
    @Override
    public PaymentResponseDTO postPayment(PaymentRequestDTO request) {
	WebTarget myResource = restClientBuilder.getClient()
		.target("http://studentpaymentserver-cgiostersund.rhcloud.com/paymentserver/rest/paymentservices/pay");

	PaymentResponseDTO response = myResource.request(MediaType.APPLICATION_JSON).post(Entity.json(request),
		PaymentResponseDTO.class);
	return response;
    }
}
