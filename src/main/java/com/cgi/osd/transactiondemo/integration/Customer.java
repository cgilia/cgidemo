package com.cgi.osd.transactiondemo.integration;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the customer database table.
 *
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
	@NamedQuery(name = "Customer.findByFirstName", query = "SELECT c FROM Customer c WHERE c.firstName = :firstName"),
	@NamedQuery(name = "Customer.findByLastName", query = "SELECT c FROM Customer c WHERE c.lastName = :lastName"),
	@NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
	@NamedQuery(name = "Customer.findByPhone", query = "SELECT c FROM Customer c WHERE c.phone = :phone"),
	@NamedQuery(name = "Customer.deleteByEmail", query = "DELETE FROM Customer AS c WHERE c.email = :email") })
@Table(name = "customer")
public class Customer extends EntityBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    // bi-directional one-to-one association to User
    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private User user;

    // bi-directional many-to-one association to Payment
    @OneToMany(mappedBy = "customerBean", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Payment> payments;

    // bi-directional many-to-one association to Seat
    @OneToMany(mappedBy = "customerBean", cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
	    CascadeType.REFRESH }, fetch = FetchType.EAGER)

    private List<Seat> seats;

    public Customer() {
    }

    public String getFirstName() {
	return this.firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return this.lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getEmail() {
	return this.email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPhone() {
	return this.phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public List<Payment> getPayments() {
	return this.payments;
    }

    public void setPayments(List<Payment> payments) {
	this.payments = payments;
    }

    public Payment addPayment(Payment payment) {
	getPayments().add(payment);
	payment.setCustomerBean(this);

	return payment;
    }

    public Payment removePayment(Payment payment) {
	getPayments().remove(payment);
	payment.setCustomerBean(null);

	return payment;
    }

    public List<Seat> getSeats() {
	return this.seats;
    }

    public void setSeats(List<Seat> seats) {
	this.seats = seats;
    }

    public Seat addSeat(Seat seat) {
	getSeats().add(seat);
	seat.setCustomerBean(this);

	return seat;
    }

    public Seat removeSeat(Seat seat) {
	getSeats().remove(seat);
	seat.setCustomerBean(null);

	return seat;
    }

    public User getUser() {
	return this.user;
    }

    public void setUser(User user) {
	this.user = user;
    }
}