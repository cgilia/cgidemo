package com.cgi.osd.transactiondemo.logic.domainobject;

import java.util.Date;
import java.util.List;

import com.cgi.osd.transactiondemo.integration.Payment;
import com.cgi.osd.transactiondemo.integration.Seat;

/**
 * This class is responsible for implementing the domain object Seat.
 *
 * @author viktorlindh
 *
 */

public class CustomerDO {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private Date date;
    private List<Seat> bookedSeats;
    private List<Payment> payments;

    public CustomerDO(String firstName, String lastName, String email, String phone, Date date, List<Seat> bookedSeats,
	    List<Payment> payments) {
	super();
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
	this.phone = phone;
	this.date = date;
	this.bookedSeats = bookedSeats;
	this.payments = payments;
    }

    public String getFirstName() {
	return firstName;
    }

    public void setFirstName(String firstName) {
	this.firstName = firstName;
    }

    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastName) {
	this.lastName = lastName;
    }

    public String getEmail() {
	return email;
    }

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPhone() {
	return phone;
    }

    public void setPhone(String phone) {
	this.phone = phone;
    }

    public Date getDate() {
	return date;
    }

    public void setDate(Date date) {
	this.date = date;
    }

    public List<Seat> getBookedSeats() {
	return bookedSeats;
    }

    public void setBookedSeats(List<Seat> bookedSeats) {
	this.bookedSeats = bookedSeats;
    }

    public List<Payment> getPayments() {
	return payments;
    }

    public void setPayments(List<Payment> payments) {
	this.payments = payments;
    }
}
