package com.cgi.osd.transactiondemo.logic;

import java.util.List;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This interface defines the method required for handling bookings.
 *
 * @author anderssonhr
 *
 */
public interface BookingService {

    /**
     * This method returns a list of all seats.
     *
     * @return a list of all seats.
     * @throws SeatException
     */
    public List<SeatDO> getAllSeats() throws PersistenceOperationException;

    List<SeatDO> checkSeatVersion(List<SeatDO> selectedSeats);

    public void createBooking(CustomerDO customerDO, List<SeatDO> selectedSeats, UserDO userDO,
	    PaymentRequestDTO paymentRequestDTO) throws PersistenceOperationException;

}
