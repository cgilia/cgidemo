/**
 *
 */
package com.cgi.osd.transactiondemo.logic.domainobject;

/**
 * This class is responsible for implementing the domain object Seat.
 *
 * @author anderssonhr
 *
 */
public class SeatDO {

    private int seatNumber;
    private int version;
    private int seatLock;

    public SeatDO() {

    }

    public SeatDO(int seatNumber, int version, int seatLock) {
	super();
	this.seatNumber = seatNumber;
	this.version = version;
	this.seatLock = seatLock;
    }

    public int getSeatNumber() {
	return seatNumber;
    }

    public int getVersion() {
	return version;
    }

    public int getSeatLock() {
	return seatLock;
    }

    public void setSeatLock(int seatLock) {
	this.seatLock = seatLock;
    }
}
