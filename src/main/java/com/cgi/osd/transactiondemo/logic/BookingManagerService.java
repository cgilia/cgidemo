package com.cgi.osd.transactiondemo.logic;

import java.util.List;

import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;

public interface BookingManagerService {

    List<CustomerDO> getAuthenticatedCustomer(String userName);

    void removeBooking(CustomerDO customerDO);

    List<CustomerDO> getAllBookings();
}
