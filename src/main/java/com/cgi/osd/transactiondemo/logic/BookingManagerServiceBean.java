package com.cgi.osd.transactiondemo.logic;

import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.integration.PersistenceManager;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;

@ApplicationScoped
public class BookingManagerServiceBean implements BookingManagerService {

    @Inject
    Logger logger;

    @Inject
    private PersistenceManager persistenceManager;

    @Override
    public List<CustomerDO> getAuthenticatedCustomer(String userName) {
	List<CustomerDO> customerDO = persistenceManager.getCustomerByEmail(userName);
	return customerDO;
    }

    @Override
    public void removeBooking(CustomerDO customerDO) {
	persistenceManager.deleteBooking(customerDO);
    }

    @Override
    public List<CustomerDO> getAllBookings() {
	final List<CustomerDO> allBookings = persistenceManager.getAllbookings();
	return allBookings;
    }
}
