package com.cgi.osd.transactiondemo.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.transactiondemo.integration.PersistenceManager;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for implementing the booking services.
 *
 * @author anderssonhr
 *
 */

@ApplicationScoped
public class BookingServiceBean implements BookingService {

    @Inject
    Logger logger;

    @Inject
    private PersistenceManager persistenceManager;

    /*
     * (non-Javadoc)
     *
     * @see com.cgi.osd.transactiondemo.logic.BookingService#getAllSeats()
     */
    @Override
    public List<SeatDO> getAllSeats() throws PersistenceOperationException {
	final List<SeatDO> seats = persistenceManager.getAllSeats();
	return seats;
    }

    @Override
    public void createBooking(CustomerDO customerDO, List<SeatDO> selectedSeats, UserDO userDO,
	    PaymentRequestDTO paymentRequestDTO) throws PersistenceOperationException {
	final int customerId = persistenceManager.insertNewCustomer(customerDO, userDO);
	persistenceManager.createBooking(customerId, convertSeatnumberToInteger(selectedSeats), paymentRequestDTO);

    }

    @Override
    public List<SeatDO> checkSeatVersion(List<SeatDO> selectedSeats) {
	List<SeatDO> selectedSeatsToCompare = persistenceManager
		.getAllSelectedSeats(convertSeatnumberToInteger(selectedSeats));
	return selectedSeatsToCompare;
    }

    public List<Integer> convertSeatnumberToInteger(List<SeatDO> selectedSeats) {
	List<Integer> seatNumbers = new ArrayList<>();
	for (SeatDO seats : selectedSeats) {
	    seatNumbers.add(seats.getSeatNumber());
	}
	return seatNumbers;
    }
}
