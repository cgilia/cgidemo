package com.cgi.osd.transactiondemo.logic;

import java.security.SecureRandom;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.base.Charsets;
import com.google.common.hash.Hashing;

public class HashPassword {

    public String generateHash(String password) {
	String hash = Hashing.sha256().hashString(password, Charsets.UTF_8).toString();
	return hash;
    }

    public String generatePassword() {
	String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	String pwd = RandomStringUtils.random(8, 0, 0, false, false, characters.toCharArray(), new SecureRandom());
	return pwd;
    }
}
