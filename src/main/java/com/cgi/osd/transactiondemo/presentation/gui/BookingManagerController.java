package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.security.Principal;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.cgi.osd.transactiondemo.logic.BookingManagerService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;

@SessionScoped
@Named
public class BookingManagerController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    Principal principal;

    @Inject
    private Logger logger;

    @Inject
    BookingManagerData bookingManagerData;

    @Inject
    BookingManagerService bookingManagerService;

    private CustomerDO selected = new CustomerDO(null, null, null, null, null, null, null);

    public List<CustomerDO> getAllBookings() {
	return this.bookingManagerData.getAllBookings();
    }

    public CustomerDO getSelected() {
	return selected;
    }

    public void setSelected(CustomerDO selected) {
	this.selected = selected;
    }

    public String getPrincipal() {
	return principal.getName();
    }

    public List<CustomerDO> getCustomerBooking() {
	return this.bookingManagerData.getCustomerBooking(getPrincipal());
    }

    //
    public void deleteBooking() {
	bookingManagerService.removeBooking(selected);
    }
}
