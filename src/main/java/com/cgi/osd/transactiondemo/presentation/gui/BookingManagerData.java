package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.cgi.osd.transactiondemo.logic.BookingManagerService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;

@Dependent
public class BookingManagerData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private Logger logger;

    @Inject
    BookingManagerService bookingManagerService;

    private List<CustomerDO> allBookings;

    private List<CustomerDO> customerBooking;

    public List<CustomerDO> getAllBookings() {
	allBookings = bookingManagerService.getAllBookings();
	return allBookings;
    }

    public List<CustomerDO> getCustomerBooking(String userName) {
	customerBooking = bookingManagerService.getAuthenticatedCustomer(userName);
	return customerBooking;
    }

    public void setCustomerBooking(List<CustomerDO> customerBooking) {
	this.customerBooking = customerBooking;
    }
}
