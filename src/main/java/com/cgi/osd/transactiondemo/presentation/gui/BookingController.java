/**
 *
 */
package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FlowEvent;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.transactiondemo.integration.EmailUtil;
import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.HashPassword;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.domainobject.UserDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for the controller part of the MVC pattern for the
 * booking process.
 *
 * @author anderssonhr
 *
 */
@SessionScoped
@Named
public class BookingController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private Logger logger;

    @Inject
    private BookingData bookingData;

    @Inject
    BookingService bookingService;

    @Inject
    EmailUtil emailUtil;

    HashPassword password = new HashPassword();

    private boolean skip;

    private String userPass = "";

    public List<SeatDO> getAllSeats() {
	return this.bookingData.getAllSeats();
    }

    public List<SeatDO> getSelectedSeats() {
	return this.bookingData.getSelectedSeats();
    }

    public void setSelectedSeats(List<SeatDO> selectedSeats) {
	this.bookingData.setSelectedSeats(selectedSeats);
    }

    public CustomerDO getCustomerDO() {
	return this.bookingData.getCustomerDO();
    }

    public void setCustomerDo(CustomerDO customerDO) {
	this.bookingData.setCustomerDO(customerDO);
    }

    public PaymentRequestDTO getPaymentRequestDTO() {
	return this.bookingData.getPaymentRequestDTO();
    }

    public void setPaymentRequestDTO(PaymentRequestDTO paymentRequestDTO) {
	this.bookingData.setPaymentRequestDTO(paymentRequestDTO);
    }

    public boolean isSkip() {
	return skip;
    }

    public void setSkip(boolean skip) {
	this.skip = skip;
    }

    public String getGeneratedRequestId() {
	String randomRequestId = UUID.randomUUID().toString().toLowerCase();
	return randomRequestId;
    }

    public String generateHashedPass(String userPass) {
	String hashedPass = password.generateHash(userPass);
	return hashedPass;
    }

    public UserDO createUserAccount() {
	userPass = password.generatePassword();
	UserDO userDO = new UserDO(getCustomerDO().getEmail(), generateHashedPass(userPass));
	return userDO;
    }

    public String createBooking() throws PersistenceOperationException {
	final double price = getSelectedSeats().size() * 200;
	getPaymentRequestDTO().setRequestId(getGeneratedRequestId());
	getPaymentRequestDTO().setAmount(price);
	if (checkIfSeatAlreadyBooked()) {
	    bookingService.createBooking(getCustomerDO(), getSelectedSeats(), createUserAccount(), getPaymentRequestDTO());
	    logger.info("Booking OK!");
	    sendConfirmationEmail();
	    return "/bookingpage" + "?faces-redirect=true";
	} else {
	    return "/failpage" + "?faces-redirect=true";
	}
    }

    public void sendConfirmationEmail() {
	emailUtil.sendEmail(getCustomerDO(), getSelectedSeats(), userPass);
    }

    public boolean checkIfSeatAlreadyBooked() {
	List<SeatDO> selectedSeatsToCompare = bookingService.checkSeatVersion(getSelectedSeats());
	for (SeatDO seatList : getSelectedSeats()) {
	    for (SeatDO versionCompareList : selectedSeatsToCompare) {
		if (!(seatList.getVersion() == versionCompareList.getVersion())) {
		    return false;
		} else {
		    return true;
		}
	    }
	}
	return false;
    }

    public String onFlowProcess(FlowEvent event) {
	if (skip) {
	    skip = false; // reset in case user goes back
	    return "confirm";
	} else {
	    return event.getNewStep();
	}
    }

    public boolean disableSeats(int seatLockNumber) {
	final int locked = 1;
	if (seatLockNumber == locked) {
	    return true;
	}
	return false;
    }

}
