package com.cgi.osd.transactiondemo.presentation.gui;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.cgi.osd.student.paymentserver.soap.PaymentRequestDTO;
import com.cgi.osd.transactiondemo.logic.BookingService;
import com.cgi.osd.transactiondemo.logic.domainobject.CustomerDO;
import com.cgi.osd.transactiondemo.logic.domainobject.SeatDO;
import com.cgi.osd.transactiondemo.logic.exception.PersistenceOperationException;

/**
 * This class is responsible for holding booking data that needs to be cached
 * for some reason. Should only be accessed using the BookingController to make
 * sure that only one instance will be connected to session.
 *
 * @author anderssonhr
 *
 */
@Dependent
public class BookingData implements Serializable {

    private static final long serialVersionUID = 1L;
    private CustomerDO customerDO = new CustomerDO(null, null, null, null, null, null, null);

    private PaymentRequestDTO paymentRequestDTO = new PaymentRequestDTO();

    @Inject
    private BookingService bookingService;

    @Inject
    private Logger logger;

    private List<SeatDO> seats;

    private List<SeatDO> selectedSeats;

    public List<SeatDO> getAllSeats() {
	return seats;
    }

    public void setSeats(List<SeatDO> seats) {
	this.seats = seats;
    }

    public List<SeatDO> getSelectedSeats() {
	return selectedSeats;
    }

    public void setSelectedSeats(List<SeatDO> selectedSeats) {
	this.selectedSeats = selectedSeats;
    }

    public CustomerDO getCustomerDO() {
	return customerDO;
    }

    public void setCustomerDO(CustomerDO customerDO) {
	this.customerDO = customerDO;
    }

    public PaymentRequestDTO getPaymentRequestDTO() {
	return paymentRequestDTO;
    }

    public void setPaymentRequestDTO(PaymentRequestDTO paymentRequestDTO) {
	this.paymentRequestDTO = paymentRequestDTO;
    }

    @PostConstruct
    private void init() {
	try {
	    seats = bookingService.getAllSeats();
	} catch (final PersistenceOperationException e) {
	    logger.severe("Failed to fetch all seats: " + e.getMessage());
	}
    }
}
